package main

import (
	"context"
	"log"

	"cloud.google.com/go/firestore"
)

type PubSubMessage struct{}

func foo(ctx context.Context, m PubSubMessage) {

	// Sets your Google Cloud Platform project ID.
	projectID := "YOUR_PROJECT_ID"

	// Get a Firestore client.
	client, err := firestore.NewClient(ctx, projectID)

	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	// Close client when done.
	defer client.Close()

}
